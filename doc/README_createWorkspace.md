# How to use config scripts for createWorkspace.cc

This script allows to create a RooWorkspace in a .root file with "heavy" objects like RooDatasets and (folded) RooKeysPdfs.
Those workspaces can subsequently be picked up by beef for fitting, allowing to make cuts on the dataset, without having to read it from the original tree (the one put into this script).<br>
Be aware to give sensible ranges to variables. *It is highly recommended to configure a the same range for observables in both configuration scripts!*

## Direct configurables:
- basiccuts : ( `string`, default `""`   )  Cut with wich the dataset will be read in (`TCut` format)

        basiccuts "K_ProbNNk > 0.01 && D0BDT > -0.03 && LcBDT > -0.03"

- dsname    : ( `string`, default `"ds"` )  name of the dataset that will be written to the output file
- suppress_RootInfo : (`int`  , optional) Suppresses ROOT messages and RooFit messages below the following levels (-1: unset) DEBUG=0, INFO=1, PROGRESS=2, WARNING=3, ERROR=4, FATAL=5
- noImplicitCuts : optional. If given, the ranges of all variables except the observable and overridden variables (see below) will be ignored

## Non-iteratable objects:
- observable: (property tree (pt) child, mandatory) Observable of the model(s). So far only one observable can be parsed. <br>
  `name` and range `[low,high]` have to be specified

        observable {
            name "mLb"
            low  5240
            high 5840
        }

- resolution: (pt child, optional) Folds imported pdfs with a Gaussian resolution which is calculated by the mean _Q_ value (mean mass - mass threshold) and a scaling coefficient as _c*sqrt(&lt;Q&gt;)_
    - MassThreshold    : ( `float`, default `0.0` ) Threshold of the observable, e.g. M(Lc)+M(D0)+M(K) for the channel Lb -> Lc+ D0-bar K-
    - ResolutionScaling: ( `float`, default `1.0` ) Factor to scale the sqrt dependence of the resolution

            resolution {
                MassThreshold     4644.977
                ResolutionScaling 0.2
            }

## Iterable objects (all Optional):
- variables: Variables which get loaded into the workspace as spectator, or for cuts. <br>
  Their name is given as object identifier, optional parameters are `low`, `high` to specify the range. <br>
  **ATTENTION** be aware that the given range is an implicit cut on the data (to avoid, use `noImplicitCuts`)

        variables {
          Lb_PT {  ;1st item in variables-list
            low 0
            high 3e+5
          }
          Lb_ETA { ;2nd item in variables-list
            low 1.9
            high 5.5
          }
          mLc ;3rd item valid from -inf to inf
        }

- variables_override: Override ranges for variables (useful when including other config file or when using `noImplicitCuts` to pass ranges for certain variables)

        variables_override {
          mLc {
            low 2258
            high 2318
          }
        }

- kdepdfs  : Pdfs added to workspace that use the `RooKeysPdf` class. <br>
  The name of the iterable object (lets call it _itername_) will be the name of the `RooKeysPdf`. <br>
  If the resolution node is given, the name will be appended with an additional `xg`. <br>
  When setting up the model with factory code use `RooKeysPdf::itername` or `RooFFTConvPdf::iternamexg`
    - filename  : ( `string`            , no default                ) Location of filename with shape for `RooKeysPdf` (absolute or relative to path given with `-d`)
    - treepath  : ( `string`            , no default                ) Location of tree within given file
    - obsinfile : ( `string`            , default `observable.name` ) Name of branch/leaf where the shape will be extracted from. Defaults to the name of the observable specified above
    - mirror    : ( `RooKeysPdf::Mirror`, default `3 (MirrorBoth)`  ) Mirror-option in `RooKeysPdf` ctor. Chose an integer number between 0 and 8 for the following options <br>
      `enum Mirror { NoMirror = 0, MirrorLeft, MirrorRight, MirrorBoth, MirrorAsymLeft, MirrorAsymLeftRight, MirrorAsymRight, MirrorLeftAsymRight, MirrorAsymBoth };`
    - rho       : ( `double`            , default `1.0`             ) Should only be adjusted in extreme situations (very strong local peaks, see [arXiv:hep-ex/0011057](https://arxiv.org/abs/hep-ex/0011057 "arXiv:hep-ex/0011057"))
    - fold      : ( `bool`              , default `1 (true)`        ) Set folding for every pdf individually

             kdepdfs {
               LcDstgK { ;1st item in kdepdfs-list
                 filename   "LcD0gK_mc.root"
                 treepath   "smalltree"
                 obsinfile  "Lb_Cons_M"
                 fold       0
               }
               LcDstpi0K { ;2nd item in kdepdfs-list
                 filename   "root://eoslhcb.cern.ch//eos/lhcb/user/m/mstahl/Lb2LcD0K/XFeedMC/15196400.root"
                 treepath   "MCDecayTreeTuple/MCDecayTree"
                 obsinfile  "Lb_LcD0K_M"
               }
             }
