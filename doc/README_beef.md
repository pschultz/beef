# How to use config scripts for beef.cc
This script is a wrapper around RooFit that allows to use it's most important features in an easy, runtime-configurable way.<br>
If you like examples better than reading through the options, here is one from the LcD0K analysis: [LcD0K-fitconfig](https://gitlab.cern.ch/lhcb-bandq-exotics/Lb2LcD0K/blob/master/config/fits/FitConfig_LcD0K.info)<br>
Be aware to give sensible ranges to variables. ***It is highly recommended to configure a the same range for observables in both configuration scripts!***

## Arguments for the executable
- `-c` : config file
- `-d` : work directory
- `-i` : input file name
- `-o` : ouput file name
- `-r` : input RooDataSet name
- `-w` : input RooWorkspace name
- `-v` : verbosity (1-3, optional)
- `-p` : prefix (gets prepended to *plotname* given in config file)
- `-h` : help
- nonoptions:
    - an optional cutstring
    - a list of `<child1:fitresfile1[:ws1]> [<child2:fitresfile2> ...]` combinations. See the [Constrain parameters](#constrain-parameters) section

## Setup a fit-model (the only mandatory thing)
beef uses workspace factory commands from RooFit internally. If you are not familiar with factory commands, check RooFit tutorials
[rf511](https://root.cern.ch/doc/master/rf511__wsfactory__basic_8C.html), [rf512](https://root.cern.ch/doc/master/rf512__wsfactory__oper_8C.html) and [rf513](https://root.cern.ch/doc/master/rf513__wsfactory__tools_8C.html). <br>
In the config file, the following is minimally needed to build models:
- model : (property tree child, ***mandatory***) List of factory commands to build the model pdf(s).
- pdf   : (`string`, ***mandatory*** ) Pdf to use in the fit. It has to be the name of the complete model given in `model`/RooFit factory settings

        model {
          Signal     "SUM::gLb(fLb[0.6,0.0,1]*Gaussian(mLb,mmLb[5620,5615,5625],sLb[7,2,15]),Gaussian(mLb,mmLb,expr('@0*@1',{sLbf[1.6,0.3,30],sLb})))"
          Background "Chebychev::bgLb(mLb,{tauLb[-0.22,-2.0,2.0]})"
          Model      "SUM::modelLb(NLb[950,0,1e+5]*gLb,NbgLb[4000,0,1e+5]*bgLb)"
        }
        pdf modelLb

In this little example, a "double-Gaussian" with common mean is constrcted as signal, a 1st order Chebychev polynomial as background, and the sum of both is a PDF with name `modelLb`.<br>
All parameters and functions are immediately imported to the current RooWorkspace. The name of the key doesn't play a role. Only the value, i.e. the factory command is relevant.

## Handle datasets
Selections can be applied by command line or config file. The command line cut is appended to the config file cut. If cuts are given in config file and command line, the proper connection between the 2 strings should be given by the user.
- selection : ( `string`, optional )  Cut with wich the dataset will be read in (`TCut` format)
- dsName : ( `string`, default `"Dred"` ) Name of `RooDataSet` with applied selection which will be saved in workspace of outputfile, default "Dred"
- weight : (`string`, optional) select weight variable for the RooDataSet
- weight_function : (`string`, optional) use weight variable in a function. for example: weight variable is efficiency, but we want the actual weight to be 1/efficiency

## In- and output
Some config file options to steer what's printed to stdout and saved to disk
- suppress_RootInfo : (`int`  , optional) Suppresses ROOT messages and RooFit messages below the following levels (-1: unset) DEBUG=0, INFO=1, PROGRESS=2, WARNING=3, ERROR=4, FATAL=5
- printFitResult : (`bool`  , optional) use [IOjuggler](https://gitlab.cern.ch/mstahl/IOjuggler/blob/master/debug_helpers.h) to print fit-results to stdout
- printCorrelation : (`bool`  , optional) use [IOjuggler](https://gitlab.cern.ch/mstahl/IOjuggler/blob/master/debug_helpers.h) to print correlation matrix to stdout
- saveFitResultsAsInfo : (`bool` or ptree-child, optional) save the fitted values and errors in an .info file which has the same name as the output file (except .info for .root). If configured as ptree-child, only the given set of paramters is saved in the tex file
- saveCorrelationsAsInfo : (`bool`, optional)  also save upper off-diagonal correlation matrix in .info file (this only works if `saveFitResultsAsInfo` is opted)
- saveFitResultsAsTeX : (`bool` or ptree-child, optional) save the fitted values and errors in a .tex file which has the same name as the output file (except .tex for .root). If configured as ptree-child, only the given set of paramters is saved in the tex file
- TeX_prefix : (`string`, optional) add a prefix to the parameter-names of the TeX file, so that multiple definitions of a parameter do not get overwritten in TeX
- no_bad_cov : (`bool`  , on by default) add MINUIT status of covariance matrix times 100 (a warning will be printed if the fit has a bad status)
- auxiliaries : (property tree child, optional) The values are taken as factory commands, the key does not have a meaning. Allows to put additional objects like RooFormulaVars into the result-workspace. In the LcD0K case this was helpful for toys. The model in LcD0K is not defined as Nsig*SigPDF + Nbkg*BkgPDF because we're also interested in the chamless and D* components. But for toys, the simple form is preferable. With the auxiliaries node, one can easily bring the existing pdfs into the simple form

## Get sweights
An *sPlot* is triggered when the `extended` child is found in the config file.
- extended : (property tree child) Defines which parameters are treated as yield parameters by the *sPlot*

        extended {
            NSig ; adds Nsig_sw column in the output dataset
            NBkg
        }

## Fit options
Some basic options to control the fit. See [RooAbsPdf documentation](https://root.cern.ch/doc/master/classRooAbsPdf.html#a8f802a3a93467d5b7b089e3ccaec0fa8 "RooAbsPdf") for details
- fitoptions : (property tree child, optional) A set of arguments that can be used in `RooAbsPdf::fitTo` <br>
    The following parameters of are configurable
    - Extended        : (`bool`  , optional) Add extended likelihood term, off by default
    - SumW2Error      : (`bool`  , optional) Switch on uncertaintes for weighted datasets
    - InitialHesse    : (`bool`  , optional) Run HESSE before MIGRAD as well, off by default    
    - Verbose         : (`bool`  , optional) Flag controls if verbose output is printed
    - WarningsOff     : (`bool`  , optional) Disable MINUIT warnings (enabled by default)
    - Timer           : (`bool`  , optional) Time CPU and wall clock consumption of fit steps, off by default
    - Strategy        : (`int`   , optional) Set Minuit strategy (0 through 2, default is 1)
    - PrintLevel      : (`int`   , optional) Set Minuit print level (-1 through 3, default is 1)
    - PrintEvalErrors : (`int`   , optional) Control number of p.d.f evaluation errors printed per likelihood evaluation. A negative value suppress output completely, a zero value will only print the error count per p.d.f component, a positive value is will print details of each error up to numErr messages per p.d.f component.
    - MinimizerType   : (`string`, optional) Choose minimization package to use. Check [RooAbsPdf documentation](https://root.cern.ch/doc/master/classRooAbsPdf.html#a8f802a3a93467d5b7b089e3ccaec0fa8 "RooAbsPdf")
    - MinimizerAlg    : (`string`, optional) Choose minimization algorithm to use. *MinimizerType has to be be given!*
    - NumCPU          : (`int`   , optional) Parallelize NLL calculation on num CPUs
    - StratCPU        : (`int`   , optional) Parallelization strategy, see [RooAbsPdf documentation](https://root.cern.ch/doc/master/classRooAbsPdf.html#a8f802a3a93467d5b7b089e3ccaec0fa8 "RooAbsPdf") for details. *NumCPU has to be given!*
    - FitAsHist       : (`bool`  , optional) Fit binned version of the dataset. ***Important:*** To parse the binning, the identifier name of the plot must have the format *observable*plot (e.g. xplot, yplot for the 2 observables defined in the example above)
    - Minos           : (pt child or `bool`, optional) If used as flag: MINOS is run after HESSE for all parameters (off by default).<br> If used as pt child: run MINOS only for given parameters

        fitoptions {
            NumCPU       8
            Extended     1
            Strategy     2
            Minos {
              NLb
            }
        }

- observables : (property tree child, mandatory if `FitAsHist` is given)

        observables {; for a 2D fit in x, y
            x
            y
        }

## Constrain parameters
There are two ways to constrain or fix parameters.
- One is using the config file directly.
- The other one needs a list of `<child1:fitresfile1[:ws1]> [<child2:fitresfile2> ...]` combinations from the command line nonoptions.
The first option needs a node in the config file like this:

        constrainParams {
          mean1 { ; name of the parameter to constrain, given in the `model` part of the fitconfig file
            value 5619.41
            error 0.04   ; if the error is 0.0, the parameter will be fixed, if it's < 0.0, only the starting parameter will be set to "value" (useful for simultaneous fits)
          }
          sigma1 {
            value 17.6
            error 0.6
          }
        }

The second option will get the values of the parameters to constrain from the workspace `wsN` (default gets parsed from `-w`) in `fitresfileN`. A node similar to the following has to be given in the config file:

        setParams {
          MC_sig_shape {             ; has to be same as child1 from nonoptions
            fLb        "LcDsfLb"     ; Gaussian constraint of fLb with mean and width read from parameter LcDsfLb in ws1 of file fitresfile1
            fixalphaLb "LcDsalphaLb" ; fix alphaLb to value read from LcDsalphaLb in the file given in the command line
          }
        }

## Simultaneous fits
Simultaneous fits are triggered by the `simultaneous` node. The individual key- value-pairs are the categories in which the input dataset will be split into. If a key starts with SplitParam, it is not considered as category.<br>
The value of `SplitParam` can be anything which a `SIMCLONE` factory command would take. See [RooFit-tutorial](https://root.cern.ch/doc/master/rf513__wsfactory__tools_8C_source.html). `SplitParamEdit` can then be any factory command.

        simultaneous {
          pass           "LcBDT_S21 > -0.15 && DsBDT_S21 > -0.25"
          both_fail      "LcBDT_S21 < -0.15 && DsBDT_S21 < -0.25"
          xor_fail       "(LcBDT_S21 < -0.15 || DsBDT_S21 < -0.25) && !(LcBDT_S21 < -0.15 && DsBDT_S21 < -0.25)"
          SplitParam     "$SplitParam({tauLb,tauLc,tauDs,efcombDs,efcombLc,NLb,NPR,fclLb,NbgLb},category[pass,both_fail,xor_fail])"
          SplitParamEdit "EDIT::model_bla(modelLb_xor, fclLb_xor=expr('D*NLb_xor',D[0,1]) )"
        }

Another option to build simultaneous models, is to not give the values of the categories, or also not even give the keys. A simultaneous model with 2 categories is then constructed from the nonoption command line cut.
By default (no category-keys in `simultaneous` node), "pass" and "fail" categories will be constructed.<br>
Fitting two categories with extended parameters cries out for measuring an efficiency. Methods to calculate data-driven efficiencies have been derived in the LcD0K analysis. These computations are triggered by an `efficiency` node in the config file. [Here's](https://gitlab.cern.ch/sneubert/DfromBBDTs/blob/master/config/efficiency/effi_LcPi.info) a full example.
The calculated efficiencies will be put into an .info file as `sig_eff` node with children `Value`, `ErrorLo`, `ErrorHi`. The .info file has the same name as the output file given to `-o`. In addition, the efficiency will be put as `RooRealVar` into the output workspace. Name here: `eff_sig`, `deff_sig_lo`, `deff_sig_hi
- efficiency : (property tree child, optional) triggers dedicated efficiency calculation <br>
    The following parameters of are configurable
    - NSigPassName     : (`string`,default: "NSig_pass") Name of parameter from which to compute efficiency (pass category)
    - NSigFailName     : (`string`,default: "NSig_fail") Name of parameter from which to compute efficiency (fail category)
    - StatisticsOption : (`int`, default: 0) Choose [`TEfficiency::EStatOption`](https://root.cern.ch/doc/master/classTEfficiency.html#af27fb4e93a1b16ed7a5b593398f86312)
    - ConfidenceLevel  : (`double`, default: 0.6827) Choose TEfficiency CL
    - alpha            : (`double`, default 0.5) For Beta-Function of Bayesian Methods in TEfficiency
    - beta             : (`double`, default 0.5) For Beta-Function of Bayesian Methods in TEfficiency
    - get_all_limits   : (`bool`, default false) Print (to stdout) and save (to .info) all possible limits provided by TEfficiency and the Wald approximation
    - NBkgPassName     : (`string`, optional) Allows to compute background efficiency and save it to output workspace
    - NBkgFailName     : (`string`, optional) Allows to compute background efficiency and save it to output workspace

Note that the total model for measuring signal efficiencies should look like `N_sig*PDF_sig + N_bkg*PDF_bkg`. Make sure to boil it down to this by summing up the respective PDFs.
A way out of having more than 2 components of which the efficiency should be measured is to run the fit several times with different definitions of signal and background,
or to use a clever combination of the simultaneous node with the auxiliaries node.

## Plotting
- plots : Configures the plotting. All colors will be given as strings and parsed by `LHCb::color`, defined in `include/lhcbStyle.C` <br>
    All options below with ***no default*** value are mandatory as soon as the corresponding node is given in the config file <br>
    To make the list easier to read, *non-iterable childs* are italic, **iterable childs** are bold
    - **A list of iterable items** : They correspond to plots one wants to draw.<br>
      The identifier name in the property tree becomes important if a binned fit should be performed. The name must have the format *observable*plot then (e.g. xplot, yplot for the 2 observables defined in the example above).<br>
      The following parameters of each projection are configurable
        - var        : ( `string`, ***no default***     ) The name of the observable as it is in the workspace
        - low        : ( `double`, ***no default***     ) Lower limit of the x-axis
        - high       : ( `double`, ***no default***     ) High limit of the x-axis
        - bins       : ( `int`,    ***no default***     ) The number of bins with which the data is plotted
        - plotname   : ( `string`, default gets parsed  ) Name of output-graphic. Default is name of ouputfile + name of observable
        - xtitle     : ( `string`, default `"M"`        ) The title on the x-axis
        - ytitle     : ( `string`, default gets parsed  ) The title on the y-axis. Gets parsed dependent on the y-maximum and binsize, e.g. "10^{3} Events/4 MeV"
        - textsize   : ( `float`,  default `0.05`       ) Textsize for axis-labels and axis-title
        - CanvXSize  : ( `int`,    default `2048`       ) X size of canvas in pixel
        - CanvYSize  : ( `int`,    default `1280`       ) Y size of canvas in pixel
        - lmargin    : ( `float`,  default `0.11`       ) Left margin
        - tmargin    : ( `float`,  default `0.01`       ) Top margin
        - rmargin    : ( `float`,  default `0.01`       ) Right margin
        - bmargin    : ( `float`,  default `0.12`       ) Bottom margin (below the pulls)
        - pullheight : ( `float`,  default `0.32`       ) Height of the pullhistogram w.r.t. the plot of the fit. Note that the bottom margin will be subtracted from that
        - category   : ( `string`, default `category`   ) Name of RooCategory object. See https://root.cern/doc/master/rf501__simultaneouspdf_8C_source.html
        - MSizeData  : ( `float`,  default gets parsed  ) Marker size data. Parsing from gStyle
        - MColorData : ( `string`, default `"black"`    ) Marker color data
        - LWidthData : ( `short`,  default gets parsed  ) Line width data
        - LColorData : ( `string`, default `"black"`    ) Line color data
        - OptionData : ( `string`, default `"e1p"`      ) Draw option data
        - DataError  : ( `int`,    default `3`          ) Error option for drawing. See https://root.cern.ch/doc/master/classRooAbsData.html#a04a1c5bab3785b72f0791d6966425d21 and https://root.cern.ch/doc/master/classRooAbsData.html#a3f1b47ae0cc4e217f514a8e4331f7e88
        - Cut        : ( `string`, ***no default***     ) Plot data associated to the category given by the string. See https://root.cern/doc/master/rf501__simultaneouspdf_8C_source.html for an exmaple. Note that the categories name is parsed from the `category` field in the info file and `cat==cat::bla` becomes just `Cut "bla"`
        - MSizePull  : ( `float`,  default gets parsed  ) Marker size pulls
        - MColorPull : ( `string`, default `"azure+3"`  ) Marker color pulls
        - LWidthPull : ( `short`,  default gets parsed  ) Line width pulls
        - LColorPull : ( `string`, default `"azure+3"`  ) Line color pulls
        - OptionPull : ( `string`, default `"e1p"`      ) Draw option pulls
        - xOffset    : ( `float`,  default `1.05`       ) X title offset
        - yOffset    : ( `float`,  default `1.2`        ) Y title offset
        - ymin       : ( `float`,  default `0.1`        ) Y range minimum (0.1 because with 0, the 0 label would be displayed and the pull pad would overlay this label)
        - ymax       : ( `float`,  default frame-max    ) Y range maximum
        - ymaxadd    : ( `float`,  default `0`          ) Adds `ymaxadd*(ymax-ymin)` to `ymax` (helpful to add space for big legends in a semi-automated way)
        - logy       : ( `bool`,   default `false`      ) Draw Y axis in log-scale (booleans are parsed from `int`'s)
        - NdivX      : ( `int`,    default gets parsed  ) Ndivisions of X axis
        - NdivY      : ( `int`,    default gets parsed  ) Ndivisions of Y axis
        - NdivPY     : ( `int`,    default gets parsed  ) Ndivisions of Y axis on pull-frame
        - ytitlePull : ( `string`, default `"Pull "`    ) Y title of pull histogram
        - PullRange  : ( `float`,  default `4.8`        ) Pulls are drawn between `-PullRange` and `PullRange`
        - HideExpX   : ( `bool`,   default `false`      ) Hide 10^X on X axis which gets drawn automatically by `TGAxis`
        - HideExpY   : ( `bool`,   default `false`      ) Hide 10^X on Y axis which gets drawn automatically by `TGAxis`
        - MaxDigits  : ( `int` ,   default gets parsed  ) Set global maximum number of digits https://root.cern.ch/doc/master/classTGaxis.html#a6b93d66237560f7b11701402a1a446ce
        - PullCC     : ( `bool`,   default `true`       ) Do small convergence check using pulls
        - PullThreshold : (`double`, default 4.0) Convergence check doesn't pass if `PullBGT` bins are above or below `PullThreshold`
        - PullBGT    : (`int`, default 2) see above
        - plotter_verbosity : (`int`, default parsed from `-v` option) obvious

        - *graphs*    : Draws projections of full model or its components.<br>
          Projections with fill style need to be drawn several times or using *stacked_graphs*.<br>
          The order matters! And the `invisible` option helps if projections should be added.<br>
          Always plot full model at the end to get correct pulls (use invisible if projection shouldn't be drawn)
            - **A list of iterable items** : They correspond to projections one wants to draw.<br>
              The identifier name in the property tree can be used to specify the component (if `components` key is not explicitly given). The following parameters of each projection are configurable
                - components    : ( `string`, node name ) Name of the component to draw. Given by pdfname in workspace (it's called above when setting up `model`/the factory commands)
                - linecolor     : ( `string`, default `"blue"` ) Linecolor of projection
                - style         : ( `short` , default `1`      ) Linestyle of projection
                - linewidth     : ( `short`,  default `3`      ) Linewidth of projection
                - fillcolor     : ( `string`, default `"blue"` ) Fillcolor of projection
                - option        : ( `string`, default `"l"`    ) Draw option
                - addto         : ( `string`, default `""`     ) Name of projection this component should be added to. These are the names specified in components + "P". The P is internally added to avoid confusing `RooPlots` with pdfs/datasets.
                - invisible     : ( `int`,    default `0`      ) Add curve to frame, but do not display. Useful in combination addto
                - vlines        : ( `bool`,   default `false`  ) Add vertical lines to y=0 at end points of curve (if this doesn't work as expected, try `manual_vlines`)
                - manual_vlines : ( `bool`,   default `false`  ) Set last point of projection to 0 (the last point is usually just a duplicate of the previous)
                - Slice         : ( `string`, optional         ) Draw PDF projection of a category given in the `category` field above. See https://root.cern.ch/doc/master/classRooAbsPdf.html#ae19cd5285edf475b744819b72d3ca517
                - ProjWData     : ( `bool`,   default `false`  ) See [RooAbsPdf::plotOn()](https://root.cern.ch/doc/master/classRooAbsPdf.html#ae19cd5285edf475b744819b72d3ca517). Automatically chosen when `Slice` is given
                - Normalize     : ( `string`, optional         ) Option for [Plotting simultaneous PDFs](#plotting-simultaneous-pdfs)

        - *stacked_graphs* : Draws projections of full model or its components by stacking them.<br>
          The components are drawn invisible and are stacked from first to last-1, then drawn persistently from last to first. It behaves a lot like the `graphs` node, except one does not need to care about invisible, addto and (manual_)vlines as this is handled automatically.


        - *legends*   : Adds legends to frame. Legends are iterable, since one may want to have more than one legend, or a legend starting on the left and continuing on the right
            - **A list of iterable items** : They correspond to legends one wants to draw.<br>
              The identifier name in the property tree is of no importance within the plotter. The following parameters of each legend are configurable
                - header : ( `string`, optional      ) Header of the legend
                - xlo    : ( `double`, no default    ) X1 position in NDC coordinates
                - xhi    : ( `double`, no default    ) X2 position in NDC coordinates
                - ylo    : ( `double`, no default    ) Y1 position in NDC coordinates
                - yhi    : ( `double`, no default    ) Y2 position in NDC coordinates
                - dtop   : ( `double`, no default    ) Distance from top margin to upper end of legend (if you don't like `yhi`)
                - height : ( `double`, no default    ) Height of legend in units of textsizes (if you don't like `ylo`. but you need to use dtop then as well!)
                - dright : ( `double`, no default    ) Distance from right margin to right end of legend (if you don't like `xhi`) (text in legend can jut out)
                - scale  : ( `double`, default `1.0` ) Scalefactor of the legend's textsize w.r.t. the textsize given in `plot.textsize` (default `plot.textsize 0.05`)
                - **A list of iterable items**: They correspond to `RooPlot` clones one wants to refer to.<br>
                  The identifier name in the property tree is the fallback solution to name (see below). The following parameters of each `RooPlot` refence are configurable
                    - name  : ( `string`, node name + "P") Name of the projection. These are the names of the pdf/dataset + "P" as suffix. The P is internally added when drawind the component to avoid confusing `RooPlots` with pdfs/datasets. If the default solution is used (parsing from the nodename), the pdf component/ dataset name can be used directly.
                      More precise: Name of the pdf is given by `plot.graphs.<child>.components`. Name of the dataset is the name of the dataset in the workspace that contains `plot.var`
                    - label : ( `string`, madatory, no default ) Label of component
                    - option: ( `double`, default `"lpf"` ) Distance from top margin to upper end of legend

        - *pullLines* : Adds lines to pull frame.
            - **A list of iterable items** : They correspond to lines which one may want to configure in different styles.
              The identifier name of the line in the pt is of no importance within the plotter. The following parameters are configurable
                - pull   : ( `double`, no default        ) Y position of the line
                - style  : ( `short`,  default `2`       ) Line style
                - width  : ( `short`,  default `1`       ) Line width
                - color  : ( `string`, default `"black"` ) Line color
                - option : ( `string`, default `"l"`     ) Draw option
        - *outputformats*
            - A list of `string`s. Their names correspond to parsable output formats of [TCanvas::SaveAs](https://root.cern.ch/doc/master/classTCanvas.html#abb7a40ea658c348cdc8f6925eb671314 "TCanvas::SaveAs")

### Plotting simultaneous PDFs
To plot simultaneous PDFs, more than one `plots` node can be given to support the auto-replace function of IOjuggler. Consider this:

        plots_pass { ; if the simultaneous node is given, beef will also search for nodes of the form `plots_`+category
          REPLACE {
            CAT pass ; replace all instances of CAT with pass in the included file below
          }
          #include "config/fits/Scan/plotconfig_sim.info"
        }
        plots_both_fail {
          REPLACE {
            CAT both_fail
          }
          #include "config/fits/Scan/plotconfig_sim.info"
        } ...

Plotting of a simultaneous PDF needs a few tweaks as well. Here's an inline-explained example:

        stacked_graphs { ; plotting components as stacked graph
          combBkg_CAT {  ; name of component as given in model node, plus appended category label "_CAT"
            ...
            Slice      CAT ; use slice option if this component has it's own name (it get's it's own name as soon as a parameter within this component is split over several categories)
          }
          Lb2LcDsst_CAT { ; if you use the auto-replace function, be sure to make a replacement in all node-names to retain the order (nodes get appended when there's a replacement in their name)
            components    "Lb2LcDsst" ; we still have this key to get the right component. so this is a component which shares its parameters over all categories
            ...
            Normalization "NPR_CAT"  ; still, it's normalization might not be shared. this allows to get a decent plot for such a case
          }
        }
        graphs {
          modelLb_CAT {
            ...
            ProjWData 1 ; the full model just needs ProjWData (don't ask me why not Slice...)
          }
        }

Last but not least: if your category name (the one given in the `SplitParams` key) is different from "category", use a `category` key as child of a plots node to give that name.
