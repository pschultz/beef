#ifndef PLOTTER_H
#define PLOTTER_H
//from std
#include <iostream>
#include <memory>
#include <string>
#include <utility>
//from BOOST
#include <boost/algorithm/string/predicate.hpp>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/range/rbegin.hpp>//not in STL yet (gcc 4.9)
#include <boost/range/rend.hpp>
//from ROOT
#include "Math/ProbFuncMathCore.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TLine.h"
#include "TString.h"
#include "TStyle.h"
//from ROOFIT
#include "RooAbsPdf.h"
#include "RooAbsData.h"
#include "RooCmdArg.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooHist.h"
#include "RooLinkedList.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"
#include "RooSimultaneous.h"
#include "RooCategory.h"
//local
#include "lhcbStyle.C"
#include "../IOjuggler/IOjuggler.h"

namespace pt = boost::property_tree;

void plotter(const RooWorkspace* w, const RooDataSet& ds, const RooAbsPdf* modelPdf, const std::string& ofn, const std::string &workdir,
             const std::string& prefix, const pt::ptree& plotnode, MessageService msgsvc){

  msgsvc.SetMsgLvl(plotnode.get("plotter_verbosity",static_cast<int>(msgsvc.GetMsgLvl())));
  //get observable
  const auto obsn = plotnode.get<std::string>("var");
  msgsvc.infomsg("Making plot of " + obsn);
  const auto Observable = get_wsobj<RooRealVar>(w,obsn);

  // set basic plot style
  lhcbStyle(static_cast<int>(msgsvc.GetMsgLvl()));

  const float textsize       = plotnode.get<float>("textsize",gStyle->GetTextSize());
  const float Left_margin    = plotnode.get<float>("lmargin",0.11);
  const float Top_margin     = plotnode.get<float>("tmargin",0.01);
  const float Right_margin   = plotnode.get<float>("rmargin",0.01);
  const float Bigy_margin    = plotnode.get<float>("bmargin",0.12);
  const float padsplit       = plotnode.get<float>("pullheight",0.32);
  const float padsplitmargin = 1e-6;//leave this hardcoded as it's the only thing making sense in this layout

  TCanvas c1("c1","Fit",plotnode.get("CanvXSize",2048),plotnode.get("CanvYSize",1280));
  TGaxis::SetMaxDigits(plotnode.get("MaxDigits",TGaxis::GetMaxDigits()));
  auto set_margins = [] (TPad& p, const float& l, const float& b, const float& r, const float& t) {
    p.SetBorderSize  (0);
    p.SetLeftMargin  (l);
    p.SetBottomMargin(b);
    p.SetRightMargin (r);
    p.SetTopMargin   (t);
  };

  TPad pad1("pad1","pad1",padsplitmargin,padsplit,1-padsplitmargin,1-padsplitmargin);
  pad1.Draw();
  pad1.cd();
  set_margins(pad1,Left_margin,padsplitmargin,Right_margin,Top_margin);
  pad1.SetTickx();
  pad1.SetTicky();
  if(plotnode.get_optional<bool>("logy"))
    pad1.SetLogy();
  c1.Update();

  //finding stuff like this costs some nerves. at least it brought me to use gdb
  RooPlot::setAddDirectoryStatus(false);
  auto plotframe = Observable->frame(plotnode.get<double>("low"), plotnode.get<double>("high"), plotnode.get<int>("bins"));

  //when working with linked lists in RooFit setProcessRecArgs(true,false); has to be called
  auto add_args_to_llist = [] (RooLinkedList& llist, std::vector<RooCmdArg*>& args) {
    for (auto arg : args){
      arg->setProcessRecArgs(true,false);
      llist.Add(arg);
    }
  };
  auto del_args = [] (std::vector<RooCmdArg*>&& args){
    for (auto arg : args)
      delete arg;
    args.clear();
  };
  //we might need a category name later. set a default one if nothing is found in the config file
  const auto catn = plotnode.get("category","category");
  //init lists for plotting ctrl
  RooLinkedList dpl;
  std::vector<RooCmdArg*> dpargs;
  dpargs.push_back(new RooCmdArg(RooFit::MarkerSize (plotnode.get("MSizeData",gStyle->GetMarkerSize()))                   ));
  dpargs.push_back(new RooCmdArg(RooFit::MarkerColor(LHCb::color(plotnode.get("MColorData","black")))                     ));
  dpargs.push_back(new RooCmdArg(RooFit::LineWidth  (plotnode.get("LWidthData",gStyle->GetLineWidth()))                   ));
  dpargs.push_back(new RooCmdArg(RooFit::LineColor  (LHCb::color(plotnode.get("LColorData","black")))                     ));
  dpargs.push_back(new RooCmdArg(RooFit::DrawOption (plotnode.get("OptionData","e1p").data())                             ));
  dpargs.push_back(new RooCmdArg(RooFit::DataError  (static_cast<RooAbsData::ErrorType>(plotnode.get<int>("DataError",3)))));
  if(const auto cts = plotnode.get_optional<std::string>("Cut"))
    dpargs.push_back(new RooCmdArg(RooFit::Cut((catn+"=="+catn+"::"+*cts).data())));
  add_args_to_llist(dpl,dpargs);

  //general setting for reasonable perpendicular edges on the error-bars
  gStyle->SetEndErrorSize(plotnode.get("EndErrorSize",3*plotnode.get("MSizeData",gStyle->GetMarkerSize())));
  ds.plotOn(plotframe,dpl);

  //add pdf projections. get set of nodes to check if they exist
  RooArgSet branchNodeSet ;
  modelPdf->branchNodeServerList(&branchNodeSet);
  //for quick and easy plotting of stacked components
  if(const auto& sg = plotnode.get_child_optional("stacked_graphs")){
    auto nsg = sg->size();
    std::vector<std::string> adds_to(nsg-1u,"");//container to store names of graphs to which the currently iterated will be added
    unsigned int stack_counter = 0;
    //plot all components as invisible (to be able to stack later)
    auto stlvit = --std::end(*sg);//damn iterators... there's probably a nice solution in boost
    for(auto vit = std::begin(*sg); vit != stlvit; ++vit){
      const auto comp_name = vit->second.get("components",vit->first);
      msgsvc.debugmsg("Adding "+comp_name+" as invisible component to the plot");
      if(branchNodeSet.find(comp_name.data()) == nullptr){
        msgsvc.debugmsg("The component " + comp_name + " is not part of the model PDF and can't be plotted");
        continue;
      }
      RooLinkedList invll;
      std::vector<RooCmdArg*> args;
      args.push_back(new RooCmdArg(RooFit::Invisible()));
      args.push_back(new RooCmdArg(RooFit::Components(comp_name.data()))); //the component has to be given!
      adds_to[stack_counter] = comp_name+"P";
      args.push_back(new RooCmdArg(RooFit::Name(adds_to[stack_counter].data())));
      if(stack_counter > 0)
        args.push_back(new RooCmdArg(RooFit::AddTo(adds_to[stack_counter-1].data(),1,1)));
      //putting the common stuff into a lambda results in seg-faults... so you'll find this part below it tree times
      const auto sla = vit->second.get_optional<std::string>("Slice");
      const auto nrm = vit->second.get_optional<std::string>("Normalization");
      const auto pwd = vit->second.get("ProjWData",0);
      if(sla || nrm || pwd){
        const auto cat = get_wsobj<RooCategory>(w,catn);
        if(sla)
          args.push_back(new RooCmdArg(RooFit::Slice(*cat,sla->data())));
        if(nrm){
          double fnorm = 0.0;
          auto titer = cat->typeIterator();
          std::string parname_without_cat;
          while(const auto vcat = titer->Next())
            if(boost::algorithm::ends_with(*nrm,static_cast<std::string>(vcat->GetName())))
              parname_without_cat = boost::algorithm::replace_last_copy(*nrm,static_cast<std::string>(vcat->GetName()),"");
          titer->Reset();
          while(const auto vcat = titer->Next())
            fnorm += get_wsobj<RooRealVar>(w,parname_without_cat+static_cast<std::string>(vcat->GetName()))->getVal();
          args.push_back(new RooCmdArg(RooFit::Normalization(get_wsobj<RooRealVar>(w,*nrm)->getVal()/fnorm,RooAbsPdf::Relative)));
        }
        args.push_back(new RooCmdArg(RooFit::ProjWData(*cat,ds)));//putting this above the ifs results in seg-fault
      }

      add_args_to_llist(invll,args);
      modelPdf->plotOn(plotframe,invll);
      del_args(std::move(args));
      stack_counter++;
    }
    //plot visible components from top to bottom
    for(auto vit = boost::rbegin(*sg); vit != boost::rend(*sg); ++vit){
      const auto comp_name = vit->second.get("components",vit->first);
      msgsvc.debugmsg("Now plotting "+comp_name);
      if(branchNodeSet.find(comp_name.data()) == nullptr){
        msgsvc.debugmsg("The component " + comp_name + " is not part of the model PDF and can't be plotted");
        continue;
      }
      RooLinkedList ll;
      std::vector<RooCmdArg*> args;
      args.push_back(new RooCmdArg(RooFit::Components(comp_name.data())));
      args.push_back(new RooCmdArg(RooFit::Name      ((comp_name + "P").data())));
      args.push_back(new RooCmdArg(RooFit::LineColor (LHCb::color((*vit).second.get("linecolor","blue"))))); //set color to blue if not specified
      args.push_back(new RooCmdArg(RooFit::LineStyle (vit->second.get("style",1))                        ));
      args.push_back(new RooCmdArg(RooFit::LineWidth (vit->second.get("linewidth",3))                    ));
      args.push_back(new RooCmdArg(RooFit::FillColor (LHCb::color((*vit).second.get("fillcolor","blue")))));
      args.push_back(new RooCmdArg(RooFit::DrawOption(vit->second.get("option","L").data())              ));
      stack_counter--;//at the last component, this will be "0u-1u"
      if(stack_counter < nsg)
        args.push_back(new RooCmdArg(RooFit::AddTo(adds_to[stack_counter].data(),1,1)));
      const auto sla = vit->second.get_optional<std::string>("Slice");
      const auto nrm = vit->second.get_optional<std::string>("Normalization");
      const auto pwd = vit->second.get("ProjWData",0);
      if(sla || nrm || pwd){
        const auto cat = get_wsobj<RooCategory>(w,catn);
        if(sla)
          args.push_back(new RooCmdArg(RooFit::Slice(*cat,sla->data())));
        if(nrm){
          double fnorm = 0.0;
          auto titer = cat->typeIterator();
          std::string parname_without_cat;
          while(const auto vcat = titer->Next())
            if(boost::algorithm::ends_with(*nrm,static_cast<std::string>(vcat->GetName())))
              parname_without_cat = boost::algorithm::replace_last_copy(*nrm,static_cast<std::string>(vcat->GetName()),"");
          titer->Reset();
          while(const auto vcat = titer->Next())
            fnorm += get_wsobj<RooRealVar>(w,parname_without_cat+static_cast<std::string>(vcat->GetName()))->getVal();
          args.push_back(new RooCmdArg(RooFit::Normalization(get_wsobj<RooRealVar>(w,*nrm)->getVal()/fnorm,RooAbsPdf::Relative)));
        }
        args.push_back(new RooCmdArg(RooFit::ProjWData(*cat,ds)));
      }

      add_args_to_llist(ll,args);
      modelPdf->plotOn(plotframe,ll);
      del_args(std::move(args));
      //manual VLines as default option
      auto gr = static_cast<TGraph*>(plotframe->findObject((comp_name + "P").data()));
      double gx = 0, gy = 0;
      gr->GetPoint(gr->GetN()-1,gx,gy);
      //The problem only is, that the last point is not set to 0
      gr->SetPoint(gr->GetN()-1,gx,0);
    }
  }

  //add non-stacked components
  for(const auto& v: plotnode.get_child("graphs")) {
    //check if component exists (no check for type yet, but this should be a corner case for our purpose)
    const auto comp_name = v.second.get("components",v.first);
    msgsvc.debugmsg("Now plotting "+comp_name);
    if(branchNodeSet.find(comp_name.data()) == nullptr){
      msgsvc.debugmsg("The component " + comp_name + " is not part of the model PDF and can't be plotted");
      continue;
    }
    RooLinkedList ledlist;
    std::vector<RooCmdArg*> args;
    if(v.second.get_optional<bool>("vlines"))    args.push_back( new RooCmdArg(RooFit::VLines()   ));
    if(v.second.get_optional<bool>("invisible")) args.push_back( new RooCmdArg(RooFit::Invisible()));
    args.push_back(new RooCmdArg(RooFit::Components(comp_name.data()))); //the component has to be given!
    args.push_back(new RooCmdArg(RooFit::Name      ((comp_name + "P").data())));
    args.push_back(new RooCmdArg(RooFit::LineColor (LHCb::color(v.second.get("linecolor","blue"))))); //set color to blue if not specified
    args.push_back(new RooCmdArg(RooFit::LineStyle (v.second.get("style",1))                      ));
    args.push_back(new RooCmdArg(RooFit::LineWidth (v.second.get("linewidth",3))                  ));
    args.push_back(new RooCmdArg(RooFit::FillColor (LHCb::color(v.second.get("fillcolor","blue")))));
    args.push_back(new RooCmdArg(RooFit::DrawOption(v.second.get("option","L").data())            ));
    args.push_back(new RooCmdArg(RooFit::AddTo     (v.second.get("addto","").data(),1,1)          ));
    const auto sla = v.second.get_optional<std::string>("Slice");
    const auto nrm = v.second.get_optional<std::string>("Normalization");
    const auto pwd = v.second.get("ProjWData",0);
    if(sla || nrm || pwd){
      const auto cat = get_wsobj<RooCategory>(w,catn);
      if(sla)
        args.push_back(new RooCmdArg(RooFit::Slice(*cat,sla->data())));
      if(nrm){
        double fnorm = 0.0;
        auto titer = cat->typeIterator();
        std::string parname_without_cat;
        while(const auto vcat = titer->Next())
          if(boost::algorithm::ends_with(*nrm,static_cast<std::string>(vcat->GetName())))
            parname_without_cat = boost::algorithm::replace_last_copy(*nrm,static_cast<std::string>(vcat->GetName()),"");
        titer->Reset();
        while(const auto vcat = titer->Next())
          fnorm += get_wsobj<RooRealVar>(w,parname_without_cat+static_cast<std::string>(vcat->GetName()))->getVal();
        args.push_back(new RooCmdArg(RooFit::Normalization(get_wsobj<RooRealVar>(w,*nrm)->getVal()/fnorm,RooAbsPdf::Relative)));
      }
      args.push_back(new RooCmdArg(RooFit::ProjWData(*cat,ds)));
    }

    add_args_to_llist(ledlist,args);
    modelPdf->plotOn(plotframe,ledlist);
    del_args(std::move(args));

    //as RooFit::VLines() doesn't do what it's supposed to, let's try something else
    if(v.second.get_optional<bool>("manual_vlines")){
      auto gr = static_cast<TGraph*>(plotframe->findObject((v.second.get<std::string>("components") + "P").data()));
      double gx = 0, gy = 0;
      gr->GetPoint(gr->GetN()-1,gx,gy);
      //The problem only is, that the last point is not set to 0
      gr->SetPoint(gr->GetN()-1,gx,0);
    }
  }

  //draw data again since pdf projections may overlay parts of the data
  dpargs.push_back(new RooCmdArg(RooFit::Name((static_cast<std::string>(ds.GetName())+"P").data())));
  dpargs.back()->setProcessRecArgs(true,false);
  dpl.Add(dpargs.back());
  ds.plotOn(plotframe,dpl);
  del_args(std::move(dpargs));

  //some axis-modifications
  plotframe->GetXaxis()->SetNoExponent(); //<-- spoils MaxDigits settings, so don't use it on other axis
  plotframe->GetXaxis()->SetLabelSize(0.0);//don't print labels
  plotframe->GetXaxis()->SetTickLength(plotnode.get("TickLengthX",plotframe->GetXaxis()->GetTickLength())/(1-padsplit));
  plotframe->GetXaxis()->SetNdivisions(plotnode.get("NdivX",gStyle->GetNdivisions("X")));
  if(plotnode.get("HideExpY",0))
    TGaxis::SetExponentOffset(1e+9,1e+9,"y");//offset = pad size * 1e+7
  plotframe->GetYaxis()->SetTickLength(plotnode.get("TickLengthY",plotframe->GetYaxis()->GetTickLength()));
  plotframe->GetYaxis()->SetTitleSize(textsize/(1-padsplit));
  plotframe->GetYaxis()->SetLabelSize(textsize/(1-padsplit));
  plotframe->GetYaxis()->SetTitleOffset(plotnode.get("yOffset",1.2)*(1-padsplit));
  const auto parsed_ymax = plotnode.get("ymax",plotframe->GetMaximum() + plotnode.get("ymaxadd",0.f)*(plotframe->GetMaximum() - plotnode.get("ymin",0.1)));
  plotframe->GetYaxis()->SetRangeUser(plotnode.get("ymin",0.1),parsed_ymax);
  const auto binsize = (plotnode.get<double>("high") - plotnode.get<double>("low"))/plotnode.get<double>("bins");
  plotframe->GetYaxis()->SetTitle(plotnode.get("ytitle",std::fabs(std::log10(parsed_ymax)) >= plotnode.get("MaxDigits",TGaxis::GetMaxDigits()) ?
                                                 static_cast<std::string>(TString::Format("10^{%d} Events/%.3g MeV",static_cast<int>(3*std::ceil((std::log10(parsed_ymax)/3.0))),binsize)) :
                                                 static_cast<std::string>(TString::Format("Events/%.3g MeV",binsize))).data());
  plotframe->GetYaxis()->SetNdivisions(plotnode.get("NdivY",gStyle->GetNdivisions("Y")));
  plotframe->SetTitle("");
  plotframe->Draw();

  //go back to canvas and start using the second pad
  c1.cd();

  TPad pad2("pad2","pad2",padsplitmargin,padsplitmargin,1-padsplitmargin,padsplit);
  set_margins(pad2,Left_margin,(Bigy_margin/padsplit),Right_margin,padsplitmargin);
  pad2.SetTickx();
  pad2.SetTicky();

  // Construct a histogram with the pulls of the data w.r.t the curve
  auto hpull = plotframe->pullHist() ;//heap https://root.cern.ch/doc/master/RooHist_8cxx_source.html#l00705
  hpull->SetMarkerSize (plotnode.get("MSizePull",gStyle->GetMarkerSize()));
  hpull->SetMarkerColor(LHCb::color(plotnode.get("MColorPull","azure+3")));
  hpull->SetLineWidth  (plotnode.get("LWidthPull",gStyle->GetLineWidth()));
  hpull->SetLineColor  (LHCb::color(plotnode.get("LColorPull","azure+3")));
  hpull->SetDrawOption (plotnode.get("OptionPull","e1p").data());

  // Create a new frame to draw the pull distribution and add the distribution to the frame
  auto pullframe = Observable->frame(plotnode.get<double>("low"),plotnode.get<double>("high"),plotnode.get<int>("bins"));
  pullframe->SetTitle(" ");
  if(plotnode.get("HideExpX",0))
    TGaxis::SetExponentOffset(1e+9,1e+9,"x");//offset = pad size * 1e+7
  pullframe->GetXaxis()->SetTitleOffset(plotnode.get("xOffset",1.05));
  pullframe->GetXaxis()->SetTitleSize  (textsize/padsplit);
  pullframe->GetXaxis()->SetLabelSize  (textsize/padsplit);
  pullframe->GetXaxis()->SetTitle      (plotnode.get("xtitle","M").data());
  pullframe->GetXaxis()->SetTickLength (plotframe->GetXaxis()->GetTickLength()/(padsplit/(1-padsplit)));
  pullframe->GetXaxis()->SetNdivisions (plotnode.get("NdivX",gStyle->GetNdivisions("X")));
  pullframe->GetYaxis()->SetLabelSize  (textsize/padsplit);
  pullframe->GetYaxis()->SetTitleSize  (textsize/padsplit);
  pullframe->GetYaxis()->SetNdivisions (plotnode.get("NdivPY",3));
  pullframe->GetYaxis()->SetTitleOffset(plotnode.get("yOffset",1.2)*padsplit);
  pullframe->GetYaxis()->SetTitle      (plotnode.get("ytitlePull","Pull ").data());
  pullframe->GetYaxis()->SetRangeUser  (-plotnode.get("PullRange",4.8),
                                        plotnode.get("PullRange",4.8));
  pullframe->addPlotable(hpull,plotnode.get("OptionPull","e1p").data());

  std::vector< std::unique_ptr<TLine> > plines;
  if(plotnode.get_child_optional("pullLines")){
    for(const auto& l : plotnode.get_child("pullLines")) {
      std::unique_ptr<TLine> pullline( new TLine(plotnode.get<double>("low"),l.second.get<double>("pull"),
                                                 plotnode.get<double>("high"),l.second.get<double>("pull")));
      pullline->SetLineStyle(l.second.get("style",2));
      pullline->SetLineWidth(l.second.get("width",gStyle->GetLineWidth()));
      pullline->SetLineColor(LHCb::color(l.second.get("color","black")));
      pullline->SetDrawOption(l.second.get("option","l").data());
      plines.push_back(std::move(pullline));
    }
  }
  for(const auto& line : plines){
    pullframe->addObject(line.get());
  }

  pad2.Draw();
  pad2.cd();
  pullframe->Draw();

  c1.cd();
  //add optional legends
  //need to store them in a vector since the legends in the loop go out of scope and cause seg-fault when frame->Draw() is called
  std::vector< std::unique_ptr<TLegend> > legs;
  if(plotnode.get_child_optional("legends")){
    for(const auto& l : plotnode.get_child("legends")) {
      const auto legxlo = l.second.get("xlo",0.0);
      const auto legylo = l.second.get_optional<double>("dtop") && l.second.get_optional<double>("height") ?
                            1-l.second.get<double>("dtop")-(l.second.get<double>("height")*textsize): l.second.get("ylo",0.0);
      const auto legxhi = l.second.get_optional<double>("dright")? 1-l.second.get<double>("dright") : l.second.get("xhi",1.0);
      const auto legyhi = l.second.get_optional<double>("dtop")  ? 1-l.second.get<double>("dtop")   : l.second.get("yhi",1.0);
      std::unique_ptr<TLegend> leg( new TLegend(legxlo,legylo,legxhi,legyhi,static_cast<std::string>(l.second.get("header","")).data(),"brNDC"));
      leg->SetBorderSize(0);
      leg->SetFillColor(kWhite);
      leg->SetTextAlign(12);
      leg->SetFillStyle(0);
      leg->SetTextSize(l.second.get<double>("scale",1.f)*textsize);
      for(const auto& v : l.second ) {
        if(const auto lbl = v.second.get_optional<std::string>("label")){
          const auto labn = v.second.get("name",v.first+"P");
          auto grp = plotframe->findObject(labn.data());
          if(grp == nullptr)
            msgsvc.debugmsg("Cannot find " + labn + " in current plot. Skipping entry");
          else
            leg->AddEntry(grp,(*lbl).data(),v.second.get("option","lpf").data());
        }
      }
      legs.push_back(std::move(leg));
    }
  }
  for(const auto& leg : legs)
    leg.get()->Draw();

  //Small convergence check
  if(plotnode.get("PullCC",1)){
    auto bins_gt_threshold = 0;
    //pulls should be within "pull_threshold" sigma
    const auto pull_threshold = plotnode.get("PullThreshold",4.0);
    bool wawo = hpull->GetY()[0] > 0;
    unsigned int npos = wawo ? 1u : 0u, nneg = wawo ? 0u : 1u;
    unsigned int wawo_counter = 1u;
    auto nbins = plotnode.get<unsigned int>("bins");
    for(auto ij = 0u; ij < nbins; ij++){
      auto pullbinval = hpull->GetY()[ij];
      if(ij > 0 && wawo != (pullbinval > 0)){
        wawo_counter++; wawo = pullbinval > 0;
      }
      wawo ? npos++ : nneg++;
      if(std::fabs(pullbinval) > pull_threshold)bins_gt_threshold++;
    }
    //number of bins that are allowed to be bigger than pull_threshold
    if(bins_gt_threshold >= plotnode.get("PullBGT",2))
      msgsvc.warningmsg("\033[0;31mPulls too high! Please have a look\033[0m");
    const auto expected_mean_wawo = static_cast<float>(2*npos*nneg)/static_cast<float>(nbins)+1.;
    const auto variance_wawo = (expected_mean_wawo-1.)*(expected_mean_wawo-2.)/(static_cast<float>(nbins-1.));
    const auto nsigma_wawo = (wawo_counter-expected_mean_wawo)/std::sqrt(variance_wawo);
    msgsvc.infomsg(TString::Format("Wald-Wolfowitz p-value of pulls: %.4f , (%.2f sigma)",ROOT::Math::normal_cdf_c(std::fabs(nsigma_wawo)),nsigma_wawo));
  }

  TString fname = static_cast<TString>(boost::algorithm::replace_all_copy(ofn,".root",""));
  //strip prepended path and add observable name
  auto autogen_plotname = static_cast<std::string>(TString::Format("%s_%s",static_cast<TObjString*>(fname.Tokenize("/")->Last())->String().Data(),Observable->GetName()).Data());
  auto plotname = prefix;
  plotname.append(plotnode.get("plotname",autogen_plotname));

  //check if plotname contains a directory and if that exists (boost::filesystem would be nice)
  dir_exists(workdir+"/"+plotname+".bla");
  auto tmpeil = gErrorIgnoreLevel;
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    gErrorIgnoreLevel = kInfo;//i still want to know where the plots have been saved
  for(const auto& ex : plotnode.get_child("outputformats"))
    c1.SaveAs((workdir+"/"+plotname+"."+ex.first).data());
  gErrorIgnoreLevel = tmpeil;

  return;
}
#endif // PLOTTER_H
