//ROOT
#include "TEntryList.h"
#include "TFile.h"
#include "TFriendElement.h"
#include "TLeaf.h"
#include "TROOT.h"
#include "TString.h"
#include "TSystem.h"
#include "TTree.h"
#include "TStopwatch.h"
//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooKeysPdf.h"
#include "RooGaussian.h"
#include "RooFFTConvPdf.h"
//C++
#include <string>
#include <vector>
#include <limits>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/replace.hpp>
//IOjuggler
#include "../IOjuggler/IOjuggler.h"
#include "../IOjuggler/ProgressBar.h"

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  auto options = parse_options(argc, argv, "c:d:i:o:t:hv:","nonoptions: any number of <file:friendtree> combinations");
  MessageService msgsvc("createWorkspace",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.infomsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  //open file, check for absolute path and a path with prepended workdir
  const auto wd = options.get<std::string>("workdir");
  auto infile = get_file(options.get<std::string>("infilename"),wd);
  auto tree = get_obj<TTree>(infile,options.get<std::string>("treename"));
  //get ptree
  pt::ptree configtree = get_ptree(options.get<std::string>("config"));

  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);
  SetRootVerbosity(configtree.get_optional<int>("suppress_RootInfo"));

  //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
  //and because they will go out of scope, we have to push them into a vector
  std::vector< TFriendElement* > fes;
  if(options.get_child_optional("extraargs")){
    for(const auto& ea : options.get_child("extraargs")){
      auto ff = get_file(static_cast<std::string>(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(0)))->String().Data()),wd);
      fes.push_back(tree->AddFriend(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(1)))->String().Data(),ff));
    }
  }

  const auto noImplicitCuts = static_cast<bool>(configtree.get("noImplicitCuts",0));
  if(noImplicitCuts) msgsvc.infomsg("Implit cuts have been switched off. This does not include the observable!");

  //apply cuts and save the indices of entries passing them (for memory-friendly processing)
  msgsvc.infomsg("Getting entrylist");
  tree->Draw(">>elist",configtree.get<std::string>("basiccuts","").data(),"entrylist");
  //get and loop this list
  TEntryList* list = static_cast<TEntryList*>(gDirectory->Get("elist"));
  tree->SetEntryList(list);
  const auto nentries = list->GetN();
  const auto bign = tree->GetEntries();
  msgsvc.infomsg(TString::Format("\"basiccuts\" selected %lld entries out of %lld (%.1f %%)",nentries,bign,100*static_cast<float>(nentries)/static_cast<float>(bign)));
  
  tree->SetBranchStatus("*",false);
  const auto ocvr = configtree.get_child_optional("variables");
  // the tenary operator is an evil hack to get the size of the vector correct if the optional variables node is missing
  std::vector<TLeaf*> var_leaves(1 + static_cast<int>(ocvr ? (*ocvr).size() : 0),nullptr);
  std::vector<std::unique_ptr<RooRealVar>> variables;
  RooArgSet vars("variables");
  //for monitoring implicit cuts
  std::vector<std::pair<std::string,unsigned int>> imp_cut_mon;

  //prepare observable
  auto obs_name = configtree.get<std::string>("observable.name");
  const auto obs_low = configtree.get<double>("observable.low");
  const auto obs_high = configtree.get<double>("observable.high");
  RooRealVar Observable(obs_name.c_str(),obs_name.c_str(),obs_low,obs_high);
  vars.add(Observable);
  variables.emplace_back(new RooRealVar(Observable));
  boost::algorithm::replace_all(obs_name, "[0]", "");
  //declare variable to check if the branch is there (a bit strangely implemented in ROOT) note: this works unless you set the status of 99999 branches at once :D
  unsigned int found = 99999, ctr = 0;
  tree->SetBranchStatus(obs_name.c_str(),true,&found);//found is set to the number of branches matching obs_name
  if(found == 99999) throw std::runtime_error("Branch "+obs_name+" not found");
  var_leaves[ctr] = tree->GetLeaf(obs_name.c_str());
  imp_cut_mon.emplace_back(std::pair<std::string,unsigned int>{obs_name,0u});

  //add "spectator" variables
  if(ocvr)
    for(const auto& v : *ocvr){
      auto name=v.first;
      auto low=v.second.get<double>("low",-std::numeric_limits<double>::infinity());//this would happen when calling RooRealVar ctor without limits
      auto high=v.second.get<double>("high",std::numeric_limits<double>::infinity());
      if(noImplicitCuts){
        low = -std::numeric_limits<double>::infinity();
        high = std::numeric_limits<double>::infinity();
      }
      //check if there is an override for this variable
      if(configtree.get_child_optional("variables_override."+name)){
        low=configtree.get<double>("variables_override."+name+".low");
        high=configtree.get<double>("variables_override."+name+".high");
        msgsvc.infomsg(TString::Format("overriding range for %s [%.5g,%.5g]",name.data(),low,high));
      }
      // switch on the branches we need:
      boost::algorithm::replace_all(name, "[0]", "");
      found = 99999;//reset it
      tree->SetBranchStatus(name.c_str(),true,&found);
      if(found == 99999) throw std::runtime_error("Branch "+name+" not found");
      std::unique_ptr<RooRealVar> myvar(new RooRealVar(name.c_str(),name.c_str(),low,high));
      const auto no_duplicate = vars.add(*myvar.get());
      if(no_duplicate){
        variables.push_back(std::move(myvar));
        var_leaves[++ctr] = tree->GetLeaf(name.c_str());
        imp_cut_mon.emplace_back(std::pair<std::string,unsigned int>{name,0u});
      }
    }

  msgsvc.infomsg("Importing data ...");
  const auto dsname = configtree.get("dsname","ds");
  RooDataSet ds(dsname.data(),dsname.data(),vars);//,RooFit::Import(*tree),RooFit::Cut(configtree.get("basiccuts","").c_str())) ;

  //loop entry list and save the new tree
  ProgressBar pg;
  pg.start(nentries,msgsvc);
  for(typename std::decay<decltype(nentries)>::type evt = 0; evt < nentries; evt++){
    tree->GetEntry(list->GetEntry(evt));
    pg.update(evt);
    auto vit = vars.createIterator();
    ctr = 0u;
    bool imp_cut = false;//handle implicit cuts given by RooRealVar-ranges
    while(auto rrv = static_cast<RooRealVar*>(vit->Next())){
      auto val = static_cast<double>(var_leaves[ctr]->GetValue(0));
      if(rrv->getMin() > val || val > rrv->getMax()){
        imp_cut = true;
        imp_cut_mon[ctr].second++;
        break;
      }
      rrv->setVal(val);
      ctr++;
    }
    if(!imp_cut) ds.add(vars);
  }
  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for filling dataset: "+std::to_string(time)+" s");

  std::sort(imp_cut_mon.begin(), imp_cut_mon.end(),[](auto a, auto b) { return b.second < a.second; });
  ctr = 1u;
  if(imp_cut_mon.front().second > 0)
    msgsvc.infomsg("Implicit cuts ranked by rejected candidates: ");
  for(const auto& mv : imp_cut_mon){
    if(mv.second == 0u)
      break;
    else if(ctr <= 5)
      msgsvc.infomsg(TString::Format("%2u. %-20.20s %6u",ctr,mv.first.data(),mv.second));
    else if(ctr == 6 && msgsvc.GetMsgLvl() < MSG_LVL::DEBUG){
      msgsvc.infomsg("Interrupting list. Switch on debug mode for full output");
      break;
    }
    else
      msgsvc.debugmsg(TString::Format("%2u. %-20.20s %6u",ctr,mv.first.data(),mv.second));
    ctr++;
  }

  if(ds.numEntries()==0) {
    msgsvc.warningmsg("0 events selected. Aborting.");
    return 1;
  }
  else{
    msgsvc.infomsg(TString::Format("Imported %d events.",ds.numEntries()));
    msgsvc.infomsg(TString::Format("Implicit cuts rejected %lld candidates out of the pre-selected %lld (%.1f %%)",
                                   nentries-ds.numEntries(),nentries,100*static_cast<float>(nentries-ds.numEntries())/static_cast<float>(nentries)));
  }
  infile->Close();
  delete infile;

  RooWorkspace w("w",true);
  w.import(ds); //,importCommands,RenameVariable("lab0_Cons_M[0]","mLb"));

  //prepare RooKeysPdfs (optional)
  if(configtree.get_child_optional("kdepdfs")){
    for(const auto& v : configtree.get_child("kdepdfs")){
      //get shapes from XFeed MC files - access the file and tree
      auto MCf = get_file(v.second.get<std::string>("filename"),wd);
      auto MCt = get_obj<TTree>(MCf,v.second.get<std::string>("treepath"));
      //load data from tree manually, since using import is in general not possible.
      //This is because the variable in MCtree should have the same name as the fit-observable
      auto MCl = MCt->GetLeaf( static_cast<std::string>(v.second.get("obsinfile",Observable.GetName())).c_str() );
      RooDataSet MCds("MCds","MCds",Observable);
      auto nEntriesMC = MCt->GetEntries();
      //for the resolution function's <Q> (see below)
      auto meanQ = 0.f; unsigned int acc = 0u;
      for (decltype(nEntriesMC) MCev = 0; MCev < nEntriesMC; MCev++){
        MCt->GetEntry(MCev);
        Observable.setVal(MCl->GetValue(0));
        if(obs_low < Observable.getVal() && Observable.getVal() < obs_high){
          MCds.add(Observable);
          meanQ += Observable.getVal();
          acc++;
        }
      }

      //make KDE-pdf from MC data
      RooKeysPdf kdepdf(v.first.c_str(),v.first.c_str(),Observable,MCds,
                        static_cast<RooKeysPdf::Mirror>(v.second.get("mirror",3)),
                        static_cast<double>(v.second.get("rho",1.f))
                        );

      //fold it with the detector resolution (optional)
      //it's not possible to directly fold a resolution function with mass dependent width https://root.cern.ch/phpBB3/viewtopic.php?t=14482
      //we keep the resolution fixed at sqrt(<Q>), where <Q> is the energy release of the sample mean of our keys pdf
      if (configtree.get_child_optional("resolution") && static_cast<bool>(v.second.get("fold",1))){
        meanQ /= static_cast<decltype(meanQ)>(acc);
        meanQ -= configtree.get("resolution.MassThreshold",0.f);
        auto resolution = sqrt(meanQ)*configtree.get("resolution.ResolutionScaling",1.f);
        msgsvc.infomsg(TString::Format("Folding %s with a %.2f MeV Gaussian resolution",kdepdf.GetName(),resolution));
        RooRealVar mg(TString::Format("%smg",kdepdf.GetName()).Data(),"mg",0);
        RooRealVar sg(TString::Format("%ssg",kdepdf.GetName()).Data(),"sg",resolution);
        RooGaussian gauss(TString::Format("%sres",kdepdf.GetName()).Data(),"Gaussian resolution",Observable,mg,sg);
        RooFFTConvPdf kxg(TString::Format("%sxg",kdepdf.GetName()).Data(),"kde (X) gauss",Observable,kdepdf,gauss);
        //w makes copy when importing. so we are safe when importing it in this scope and writing it later to file
        w.import(kxg);
      }
      else
        w.import(kdepdf);
      MCf->Close();
    }
  }

  if(msgsvc.GetMsgLvl() > MSG_LVL::INFO)
    w.Print();
  w.writeToFile((wd+"/"+options.get<std::string>("outfilename")).data());
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO) clock.Print();
  return 0;
}
