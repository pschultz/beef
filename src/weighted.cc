//ROOT
#include "TROOT.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TAxis.h"
#include "TH1.h"
#include "TH2F.h"
#include "TH3F.h"
//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooBinning.h"
#include "RooStats/SPlot.h"

//C++
#include <string>
#include <iostream>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/lexical_cast.hpp>
//IOjuggler
#include "../IOjuggler/IOjuggler.h"

namespace pt = boost::property_tree;
using namespace RooFit ;
using namespace RooStats ;

int main(int argc, char** argv){

  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  auto options = parse_options(argc, argv, "c:i:o:hw:r:d:");
  //open file, check for absolute path and a path with prepended workdir
  auto infile = get_file(options.get<std::string>("infilename"),options.get<std::string>("workdir"));
  //get workspace from infile
  auto ws = get_obj<RooWorkspace>(infile,options.get<std::string>("wsname"));
  infile->Close();
  //get ptree
  pt::ptree configtree = get_ptree(options.get<std::string>("config"));
  //get dataset
  auto ds = get_wsobj<RooDataSet>(ws,options.get<std::string>("dsname"));

  // get s-weighted data
 RooDataSet * dataw = new RooDataSet(ds->GetName(),ds->GetTitle(),ds,*ds->get(),0,configtree.get<std::string>("weights").c_str()) ; // need to configure this from property tree
  std::cout<<dataw->numEntries()<<std::endl;

  TFile* ofile = TFile::Open( options.get<std::string>("outfilename").data() ,"RECREATE");
  ofile->cd();

  // make the 1D plots
  if(configtree.get_child_optional("plots")){
    for(const auto& pc : configtree.get_child("plots")){
      std::string variable=pc.first.data();
      std::cout<<variable<<std::endl;
      auto var = get_wsobj<RooRealVar>(ws,variable);
      RooPlot* plot=var->frame();
      dataw->plotOn(plot,Binning(pc.second.get<unsigned int>("bins",100u)),DataError(RooAbsData::SumW2));
      plot->SetTitle(pc.second.get<std::string>("title",static_cast<std::string>("")).c_str());
      plot->SetName(variable.c_str());
      plot->GetXaxis()->SetTitle(pc.second.get<std::string>("xtitle",static_cast<std::string>("")).c_str());

      TString histoname = "h1_" + variable;
      TH1* histo=dataw->createHistogram("",*var,Binning(pc.second.get<unsigned int>("bins",100u)));
      histo->SetTitle(pc.second.get<std::string>("title",static_cast<std::string>("")).c_str());
      histo->SetName(histoname);
      histo->GetXaxis()->SetTitle(pc.second.get<std::string>("xtitle",static_cast<std::string>("")).c_str());

      histo->Write();
      plot->Write();
    } //  end loop over plots
  }






  // make the 1D plots with binning defined in config file
  if(configtree.get_child_optional("plots_binning")){
 
    //need to get unweighted dataset
    RooDataSet * data_unweighted = new RooDataSet(ds->GetName(),ds->GetTitle(),ds,*ds->get(),0,"") ;

    std::vector<TH1D*> histos;
    std::vector<std::string> vars;
    for(const auto& pc : configtree.get_child("plots_binning")){
      std::string variable=pc.first.data();
      std::cout<<variable<<std::endl;
      unsigned int nbins = pc.second.get<unsigned int>("nbins",100u);
      double xlow = pc.second.get<double>("min",0);
      double xhigh = pc.second.get<double>("max",100);
      histos.push_back(new TH1D(("hbin_" + variable).c_str(), ("hbin_" + variable).c_str(), nbins, xlow, xhigh)); 
      vars.push_back(variable);
      histos.back()->GetXaxis()->SetTitle(pc.second.get<std::string>("xtitle",static_cast<std::string>("")).c_str());
      histos.back()->SetTitle(pc.second.get<std::string>("title",static_cast<std::string>("")).c_str());
     } //end loop over histos


    int entries = data_unweighted->numEntries();

    for(int j_entry = 0; j_entry < entries; j_entry++){

        auto row = data_unweighted->get(j_entry);
            
       RooRealVar * weight = (RooRealVar*) row->find(configtree.get<std::string>("weights").c_str());
       for(unsigned int i = 0; i < histos.size(); i++){
         RooRealVar * value = (RooRealVar*) row->find(vars.at(i).c_str());
         histos.at(i)->Fill(value->getVal(), weight->getVal());

       }   //  end loop over histos


   }// end loop over data set

       for(unsigned int i = 0; i < histos.size(); i++){
         histos.at(i)->Write();

       }   //  end loop over histos


  }



  // make the 1D plots with custom binning, specify all bin boundaries
  if(configtree.get_child_optional("plots_custom_bins")){
    for(const auto& pc : configtree.get_child("plots_custom_bins")){
      std::string variable=pc.first.data();
      std::cout<<variable<<std::endl;
      auto var = get_wsobj<RooRealVar>(ws,variable);
      RooPlot* plot=var->frame();
      double xlow = pc.second.get<double>("xlow",0);
      double xhigh = pc.second.get<double>("xhigh",100);
      std::vector<double> boundaries;
      pt::ptree c_boundaries = pc.second.get_child("boundaries");
      for(const auto& pc_bin : c_boundaries){
        double boundary = boost::lexical_cast<double>(pc_bin.first);
        boundaries.push_back(boundary);
      }
      RooBinning custom_binning(xlow,xhigh);
      for(auto const& boundary : boundaries)
        custom_binning.addBoundary(boundary);
      dataw->plotOn(plot,Binning(custom_binning));
      plot->SetTitle(pc.second.get<std::string>("title",static_cast<std::string>("")).c_str());
      TString plot_name = variable + "_custom_binning";
      plot->SetName(plot_name);
      plot->GetXaxis()->SetTitle(pc.second.get<std::string>("xtitle",static_cast<std::string>("")).c_str());
      plot->Write();

      TString histoname = "h1_" + variable + "_custom_binning";
      TH1* histo = dataw->createHistogram("",*var,Binning(custom_binning));
      histo->SetTitle(pc.second.get<std::string>("title",static_cast<std::string>("")).c_str());
      histo->SetName(histoname);
      histo->GetXaxis()->SetTitle(pc.second.get<std::string>("xtitle",static_cast<std::string>("")).c_str());

      histo->Write();
    } //  end loop over plots
  }


  
  // make 2D plots
  if(configtree.get_child_optional("plots2D")){
    for(const auto& pc2D : configtree.get_child("plots2D")){
      std::string nameStr = pc2D.first.data();
      TString name = "h2_" + nameStr;
      std::cout<<name<<std::endl;
      auto varx=get_wsobj<RooRealVar>(ws,pc2D.second.get<std::string>("varx"));
      auto vary=get_wsobj<RooRealVar>(ws,pc2D.second.get<std::string>("vary"));
      TH2F* plot=dataw->createHistogram(*varx,*vary,pc2D.second.get<unsigned int>("binsx",100u),pc2D.second.get<unsigned int>("binsy",100u));

      plot->SetTitle(pc2D.second.get<std::string>("title",static_cast<std::string>("")).c_str());
      plot->SetName(name);
      plot->GetXaxis()->SetTitle(pc2D.second.get<std::string>("xtitle",static_cast<std::string>("")).c_str());
      plot->GetYaxis()->SetTitle(pc2D.second.get<std::string>("ytitle",static_cast<std::string>("")).c_str());

      plot->Write();
    } //  end loop over plots
  }



  // make 2D plots
  if(configtree.get_child_optional("plots2D_binning")){
    for(const auto& pc2D : configtree.get_child("plots2D_binning")){
      std::string nameStr = pc2D.first.data();
      TString name = "h2bin_" + nameStr;
      std::cout<<name<<std::endl;
      auto varx=get_wsobj<RooRealVar>(ws,pc2D.second.get<std::string>("varx"));
      auto vary=get_wsobj<RooRealVar>(ws,pc2D.second.get<std::string>("vary"));
      int binsx = pc2D.second.get<unsigned int>("binsx",100u);
      int binsy = pc2D.second.get<unsigned int>("binsy",100u);
      double xlow = pc2D.second.get<double>("xlow",0);
      double xhigh = pc2D.second.get<double>("xhigh",100);
      double ylow = pc2D.second.get<double>("ylow",0);
      double yhigh = pc2D.second.get<double>("yhigh",100);
      TH2F* plot=(TH2F*) dataw->createHistogram("",*varx,Binning(binsx, xlow, xhigh), YVar(*vary,Binning(binsy, ylow, yhigh)));

      plot->SetTitle(pc2D.second.get<std::string>("title",static_cast<std::string>("")).c_str());
      plot->SetName(name);
      plot->GetXaxis()->SetTitle(pc2D.second.get<std::string>("xtitle",static_cast<std::string>("")).c_str());
      plot->GetYaxis()->SetTitle(pc2D.second.get<std::string>("ytitle",static_cast<std::string>("")).c_str());

      plot->Write();
    } //  end loop over plots
  }


  // make 3D plots, use options of plots_binning for x axis
  if(configtree.get_child_optional("plots3D") && configtree.get_child_optional("plots_binning")){
      std::cout<< configtree.get<double>("plots3D.binsy") <<std::endl;
      unsigned int binsy = configtree.get<unsigned int>("plots3D.binsy");
      unsigned int binsz = configtree.get<unsigned int>("plots3D.binsz");
      double ylow = configtree.get<double>("plots3D.ylow");
      double yhigh = configtree.get<double>("plots3D.yhigh");
      double zlow = configtree.get<double>("plots3D.zlow");
      double zhigh = configtree.get<double>("plots3D.zhigh");
      std::string yvar = configtree.get<std::string>("plots3D.yvar");
      std::string zvar = configtree.get<std::string>("plots3D.zvar");

    for(const auto& pc3D : configtree.get_child("plots_binning")){
      std::string nameStr = pc3D.first.data();
      TString name = "h3bin_" + nameStr;
      std::cout<<name<<std::endl;
      auto varx=get_wsobj<RooRealVar>(ws,nameStr);
      auto vary=get_wsobj<RooRealVar>(ws,yvar);
      auto varz=get_wsobj<RooRealVar>(ws,zvar);

      int binsx = pc3D.second.get<unsigned int>("binsx",100u);
      double xlow = pc3D.second.get<double>("xlow",0);
      double xhigh = pc3D.second.get<double>("xhigh",100);

      TH3F* plot=(TH3F*) dataw->createHistogram("",*varx,Binning(binsx, xlow, xhigh), YVar(*vary,Binning(binsy, ylow, yhigh)), ZVar(*varz,Binning(binsz, zlow, zhigh)));

      plot->SetTitle(pc3D.second.get<std::string>("title",static_cast<std::string>("")).c_str());
      plot->SetName(name);


      plot->Write();
    } //  end loop over plots
  }




  
  ofile->Close();
  
  return 0;
}
