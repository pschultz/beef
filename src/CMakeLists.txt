#///////////////////////////////////////////////////////////////////////////
#//
#//    Copyright 2015
#//
#///////////////////////////////////////////////////////////////////////////
#//-------------------------------------------------------------------------
#//
#// Description:
#//      build file for the fitter
#//
#//
#// Author List:
#//      Sebastian Neubert    Heidelberg University       (original author)
#//
#//
#//-------------------------------------------------------------------------


message(STATUS "")
message(STATUS ">>> Setting up 'beef' directory.")

if(ROOT_INCLUDE_DIR)
  set(MY_ROOT_INCS ${ROOT_INCLUDE_DIR})
  set(MY_ROOT_LIBS ${ROOT_LIBS})
elseif(ROOT_INCLUDE_DIRS)
  set(MY_ROOT_INCS ${ROOT_INCLUDE_DIRS})
  set(MY_ROOT_LIBS ${ROOT_LIBRARIES})
else()
  message(FATAL_ERROR "ROOT environment not set")
endif()

# set include directories
set(INCLUDE_DIRECTORIES
        ${MY_ROOT_INCS}
	${CMAKE_CURRENT_SOURCE_DIR}
        ${BOOST_INCLUDE_DIR}
	)
include_directories(${INCLUDE_DIRECTORIES})


# source files that are compiled into library
set(SOURCES
        	
	)


# ROOT dictionary
set(FIT_DICTIONARY ${CMAKE_CURRENT_BINARY_DIR}/FitDict.cc)
set(FIT_DICT_INCLUDE_DIRS
        ${MY_ROOT_INCS}
        ${UTILITIES_INCLUDE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}
        )
#root_generate_dictionary(
#        "${FIT_DICTIONARY}"
#        "${FIT_DICT_INCLUDE_DIRS}"
#        
#        )

#set(SOURCES ${SOURCES} ${FIT_DICTIONARY})


# library
set(THIS_LIB "beef")
#set(GSL_LIBS
#	libgsl.a
#	libgslcblas.a
#	)
#make_shared_library(
#	"${THIS_LIB}"#
#	"${SOURCES}"
#	)

#get git hash to be used for printing in executables
if(GIT_FOUND)
        execute_process(
                COMMAND ${GIT_EXECUTABLE} --git-dir=${CMAKE_CURRENT_SOURCE_DIR}/../.git rev-parse HEAD
                OUTPUT_VARIABLE BEEF_GIT_HASH
                RESULT_VARIABLE _BEEF_GIT_LOG_RETURN
                OUTPUT_STRIP_TRAILING_WHITESPACE
        )
        if(NOT _BEEF_GIT_LOG_RETURN)
                message(STATUS "beef's git repository hash is '${BEEF_GIT_HASH}'.")
        else()
                message(STATUS "Error running 'git'. Repository hash unknown.")
        endif()
        execute_process(
                COMMAND ${GIT_EXECUTABLE} --git-dir=${CMAKE_CURRENT_SOURCE_DIR}/../IOjuggler/.git rev-parse HEAD
                OUTPUT_VARIABLE IOJ_GIT_HASH
                RESULT_VARIABLE _IOJ_GIT_LOG_RETURN
                OUTPUT_STRIP_TRAILING_WHITESPACE
        )
        if(NOT _IOJ_GIT_LOG_RETURN)
                message(STATUS "IOjuggler's git repository hash is '${IOJ_GIT_HASH}'.")
        else()
                message(STATUS "Error running 'git'. Repository hash unknown.")
        endif()
endif()
if(BEEF_GIT_HASH)
        add_definitions(-DBEEF_GIT_HASH=\"${BEEF_GIT_HASH}\")
else()
        add_definitions(-DBEEF_GIT_HASH=\"\")
endif()
if(IOJ_GIT_HASH)
        add_definitions(-DIOJ_GIT_HASH=\"${IOJ_GIT_HASH}\")
else()
        add_definitions(-DIOJ_GIT_HASH=\"\")
endif()


# executables
make_executable(createWorkspace createWorkspace.cc ${MY_ROOT_LIBS} )
make_executable(beef beef.cc ${MY_ROOT_LIBS} )
make_executable(weighted weighted.cc ${MY_ROOT_LIBS} )
make_executable(sweightingCompare sweightingCompare.cpp ${MY_ROOT_LIBS} )

if(DEFINED BEEF_PLOT)
  make_executable(beefPlot beefPlot.cpp ${MY_ROOT_LIBS})
endif()
