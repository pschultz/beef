#!/bin/bash

# set the compiler
export CC=gcc
export CXX=g++

# set up ROOT
lb-run --sh ROOT | \grep 'export PATH=' | echo "$(</dev/stdin)" | rev | cut -c 2- | rev | echo "$(</dev/stdin):\$PATH'\n" > tmpenv.sh
lb-run --sh ROOT | \grep 'export LD_LIBRARY_PATH=' | echo "$(</dev/stdin)" | rev | cut -c 2- | rev | echo "$(</dev/stdin):\$LD_LIBRARY_PATH'\n" >> tmpenv.sh

lb-run --sh ROOT | \grep 'export PATH=' | awk '{split($0,a,":")}; END {for(i in a) print a[i]};' | \grep ROOT | echo ". $(</dev/stdin)/thisroot.sh" >> tmpenv.sh
. tmpenv.sh
rm tmpenv.sh

# activate the snake environment
source activate snake
